import React, { StrictMode } from "react";
import ReactDOM from "react-dom";
import App from "./App";

ReactDOM.render(
  <StrictMode>
    <div>
      <App />
    </div>
  </StrictMode>,

  document.getElementById("root")
);
