import React, { Component } from "react";
import MyForm from "./component/MyForm";
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import NavBar from "./component/NavBar";
import MyTable from "./component/MyTable";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isError: {
        usernameErr: "",
        emailErr: "",
        passwordErr: "",
      },
      tempAcc: {
        username: "",
        email: "",
        password: "",
        gender: "",
        status: true,
      },
      account: [],
    };
  }

  onHandleText = (e) => {
    let temp = { ...this.state.tempAcc };
    let name = e.target.value;
    let check = e.target.name;
    if (check === "username") temp.username = name;
    else if (check === "email") temp.email = name;
    else if (check === "password") temp.password = name;
    else temp.gender = name;
    this.setState({
      tempAcc: temp,
    });
  };

  onHendleText = (e) => {
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        if (e.target.username === "username") {
          let pattern0 = /[^a-z]/gi;
          let result0 = pattern0.test(this.state.account.username);

          if (e.target.name) {
            if (result0) {
              this.setState({
                usernameErr: "",
              });
            } else if (this.state.username === "") {
              this.setState({
                usernameErr: "Username cannot be empty!",
              });
            } else {
              this.setState({
                usernameErr: "Incorrect username!",
              });
            }
          }
        }

        if (e.target.name === "email") {
          let pattern = /^\S+@\S+\.[a-z]{3}$/g;
          let result = pattern.test(this.state.account.email.trim());

          if (result) {
            this.setState({
              emailErr: "",
            });
          } else if (this.state.email === "") {
            this.setState({
              emailErr: "Email cannot be empty!",
            });
          } else {
            this.setState({
              emailErr: "Incorrect email!",
            });
          }
        }

        if (e.target.name === "password") {
          let pattern2 =
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*)(?=.*[_\W])[a-zA-Z0-9_\W]{8,}$/g;
          let result2 = pattern2.test(this.state.password);

          if (result2) {
            this.setState({
              passwordErr: "",
            });
          } else if (this.state.password === "") {
            this.setState({
              passwordErr: "Password cannot be empty!",
            });
          } else {
            this.setState({
              passwordErr: "Incorrect password!",
            });
          }
        }
      }
    );
  };

  onSave = () => {
    let temp = [...this.state.account];
    let items = { ...this.state.tempAcc };
    temp.push(items);
    this.setState({
      account: temp,
    });
  };

  onSelect = (index) => {
    let isCheck = this.state.account;
    let check = isCheck[index].status;
    if (check) {
      check = false;
    } else check = true;

    isCheck[index].status = check;
    this.setState({
      account: isCheck,
    });
  };

  onDelete = () => {
    let temp = this.state.account.filter((item) => item.status === true);
    this.setState({
      account: temp,
    });
  };
  render() {
    return (
      <div>
        <MDBContainer>
          <NavBar />
        </MDBContainer>
        <MDBContainer>
          <MDBRow>
            <MDBCol md="4">
              <MyForm
                items={this.state.account}
                onHandleText={this.onHandleText}
                onSave={this.onSave}
              />
            </MDBCol>
            <MDBCol md="8">
              <MyTable
                items={this.state.account}
                onSelect={this.onSelect}
                onDelete={this.onDelete}
              />
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }
}