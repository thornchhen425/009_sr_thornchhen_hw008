import React, { Component } from "react";
import {Navbar} from 'react-bootstrap'

export default class NavBar extends Component {
  render() {
    return (
      <Navbar>
        <Navbar.Brand href="#home">KSHRD Student</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            Signed in as: <a href="#login">Thorn Chhen</a>
          </Navbar.Text>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
