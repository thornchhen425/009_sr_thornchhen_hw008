import { Form, Button, Image } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

export default function MyForm(props) {
  return (
    // Image
    <Form style={{ marginTop: 50 }}>
      <Image
        src="/images/person.svg"
        alt="user"
        style={{ width: 120, height: 120, marginLeft: 115 }}
      />
      <h3 style={{ textAlign: "center" }}>Create Account</h3>

      {/* Username */}
      <Form.Group controlId="formBasicEmail">
        <Form.Label>
          <strong>Username</strong>
        </Form.Label>
        <Form.Control
          onChange={(e) => props.onHandleText(e)}
          name="username"
          type="text"
          placeholder="username"
        />
        <Form.Text className="text-muted">
          <p style={{ color: "red" }}>{props.items.usernameErr}</p>
        </Form.Text>
      </Form.Group>

      {/* Gender */}
      <Form.Label>
        <strong>Gender</strong>
      </Form.Label>
      <Form.Group>
        <Form.Check
          onChange={(e) => props.onHandleText(e)}
          custom
          inline
          label="male"
          value="male"
          type="radio"
          id="male"
          name="gender"
          
        />
        <Form.Check
          onChange={(e) => props.onHandleText(e)}
          custom
          inline
          label="female"
          value="female"
          type="radio"
          id="female"
          name="gender"
        />
      </Form.Group>

      {/* Email */}
      <Form.Group controlId="formBasicEmail">
        <Form.Label>
          <strong>Eamil</strong>
        </Form.Label>
        <Form.Control
          name="email"
          onChange={(e) => props.onHandleText(e)}
          type="email"
          placeholder="email"
        />
        <Form.Text className="text-muted">
          <p style={{ color: "red" }}>{props.items.emailErr}</p>
        </Form.Text>

        {/* Password */}
      </Form.Group>
      <Form.Group controlId="formBasicEmail">
        <Form.Label>
          <strong>Password</strong>
        </Form.Label>
        <Form.Control
          name="password"
          onChange={(e) => props.onHandleText(e)}
          type="password"
          placeholder="password"
        />
        <Form.Text className="text-muted">
            <p style={{ color: "red" }}>{props.items.passwordErr}</p>
          </Form.Text>
      </Form.Group>

      {/* Save Button */}
      <Button disabled={props.validateBtn} onClick={props.onSave} variant="primary" type="button">
        Save
      </Button>
    </Form>
  );
}
