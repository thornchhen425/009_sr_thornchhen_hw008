import { Table, Button } from "react-bootstrap";

export default function MyTable(props) {
  return (
    
    <>
      <h3 style={{ marginTop: 200 }}>Table Accounts</h3>
      <Table striped bordered hover >
        <thead>
          <tr>
            <th>#</th>
            <th>Username</th>
            <th>Eamil</th>
            <th>Gender</th>
          </tr>
        </thead>
        <tbody>
          {props.items.map((item, index) => (
            <tr
              style={{ backgroundColor: item.status ? "gray" : "red" }}
              key={index}
              onClick={() => props.onSelect(index)}
            >
              <td>{index + 1}</td>
              <td>{item.username}</td>
              <td>{item.email}</td>
              <td>{item.gender}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      <Button onClick={props.onDelete} variant="danger">
        Delete
      </Button>
    </>
  );
}
